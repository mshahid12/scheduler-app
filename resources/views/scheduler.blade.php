@extends('layouts.app')

@section('content')

        <!doctype html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Scheduler</title>
</head>

<script src="/js/dhtmlxScheduler/codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="/js/dhtmlxScheduler/codebase/dhtmlxscheduler.css" type="text/css" media="screen"
      title="no title" charset="utf-8">

<style type="text/css" media="screen">
    html, body {
        margin: 0px;
        padding: 0px;
        height: 100%;
        overflow: hidden;
    }

    .dhx_cal_event div.dhx_footer,
    .dhx_cal_event.past_event div.dhx_footer,
    .dhx_cal_event.event_high div.dhx_footer,
    .dhx_cal_event.event_low div.dhx_footer,
    .dhx_cal_event.event_medium div.dhx_footer{
        background-color: transparent !important;
    }
    .dhx_cal_event .dhx_body{
        -webkit-transition: opacity 0.1s;
        transition: opacity 0.1s;
        opacity: 0.7;
    }
    .dhx_cal_event .dhx_title{
        line-height: 12px;
    }
    .dhx_cal_event_line:hover,
    .dhx_cal_event:hover .dhx_body,
    .dhx_cal_event.selected .dhx_body,
    .dhx_cal_event.dhx_cal_select_menu .dhx_body{
        opacity: 1;
    }

    .dhx_cal_event.event_high div, .dhx_cal_event_line.event_math{
        background-color: red !important;
        border-color: #a36800 !important;
    }
    .dhx_cal_event_clear.event_high{
        color:red !important;
    }

    .dhx_cal_event.event_medium div, .dhx_cal_event_line.event_science{
        background-color: green !important;
        border-color: #698490 !important;
    }
    .dhx_cal_event_clear.event_medium{
        color:green !important;
    }

    .dhx_cal_event.event_low div, .dhx_cal_event_line.event_english{
        background-color: blue !important;
        border-color: #839595 !important;
    }
    .dhx_cal_event_clear.event_low{
        color:blue !important;
    }

    ul.tab {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }


    ul.tab li {float: left;}

    ul.tab li a {
        display: inline-block;
        color: black;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        transition: 0.3s;
        font-size: 17px;
    }

    ul.tab li a:hover {background-color: #ddd;}

    ul.tab li a:focus, .active {background-color: #ccc;}

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #my_form {
        position: absolute;
        top: 100px;
        left: 200px;
        z-index: 10001;
        display: none;
        background-color: white;
        border: 2px outset gray;
        padding: 20px;
        font-family: Tahoma;
        font-size: 10pt;
    }

    #my_form label {
        width: 200px;
    }
</style>

<style type="text/css" media="screen">
    html, body {
        margin: 0px;
        padding: 0px;
        height: 100%;
        overflow: hidden;
    }
</style>

<script type="text/javascript" charset="utf-8">
    function init() {
        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
        scheduler.config.details_on_dblclick = true;
        scheduler.config.details_on_create = true;

        scheduler.templates.event_class=function(start, end, event) {
            var css = "";

            if (event.priority)
                css += "event_" + event.priority;

            if (event.id == scheduler.getState().select_id) {
                css += " selected";
            }
            return css;
        };

        scheduler.init('scheduler_here', new Date(), "month");
        scheduler.load("data/connector.php");

        var dp = new dataProcessor("data/connector.php");
        dp.init(scheduler);

    }
    var html = function (id) {
        return document.getElementById(id);
    };

    scheduler.showLightbox = function (id) {
        var ev = scheduler.getEvent(id);
        scheduler.startLightbox(id, html("my_form"));

        html("eventname").focus();
        html("eventname").value = ev.text;
        html("priority").value = ev.priority || "";
        html("fname").value = ev.fname || "";
        html("lname").value = ev.lname || "";
        html("email").value = ev.email || "";
        html("address").value = ev.address || "";
        html("nummer").value = ev.nummer || "";
        html("carreg").value = ev.carreg || "";
        html("vin").value = ev.vin || "";
        html("chassis").value = ev.chassis || "";
        html("oil").value = ev.oil || "";
        html("modelnum").value = ev.modelnum || "";
        html("lastserv").value = ev.lastserv || "";
        html("inscom").value = ev.inscom || "";
        html("notes").value = ev.notes || "";

    };

    function save_form() {
        var ev = scheduler.getEvent(scheduler.getState().lightbox_id);
        ev.eventname = html("eventname").value;
        ev.priority = html("priority").value;
        ev.fname = html("fname").value;
        ev.lname = html("lname").value;
        ev.email = html("email").value;
        ev.address = html("address").value;
        ev.nummer = html("nummer").value;
        ev.carreg = html("carreg").value;
        ev.vin = html("vin").value;
        ev.chassis = html("chassis").value;
        ev.oil = html("oil").value;
        ev.modelnum = html("modelnum").value;
        ev.lastserv = html("lastserv").value;
        ev.inscom = html("inscom").value;
        ev.notes = html("notes").value;

        scheduler.endLightbox(true, html("my_form"));

        var $end_date = html("end_date").value;
        var $email = ev.email;

        console.log($end_date);
        console.log($email);

        $.ajax({
            url: 'enddate.php',
            data: {x: $end_date,y: $email},
            type: 'POST'
        });

        location.reload();
    }
    function close_form() {
        scheduler.endLightbox(false, html("my_form"));
    }

    function delete_event() {
        var event_id = scheduler.getState().lightbox_id;
        scheduler.endLightbox(false, html("my_form"));
        scheduler.deleteEvent(event_id);
    }
    function auto(evt, customer_name) {
        var i, tabcontent, tablinks;


        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        document.getElementById(customer_name).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>

<body onload="init();">

<div id="my_form">
    <ul class="tab">
        <li><a href="#" class="tablinks" onclick="auto(event, 'customer')">Customer Data</a></li>
        <li><a href="#" class="tablinks" onclick="auto(event, 'vehicle')">Vehicle Data</a></li>
    </ul>

    <div id="customer" class="tabcontent">
        <br>

        Priority
        <label for="priorty"></label><label>
            <select name="priority" id="priority">
                <option value="high">High</option>
                <option value="medium">Medium</option>
                <option value="low">Low</option>
            </select>
        </label><br><br>

        <label for="eventname">Event Name </label><input type="text" name="eventname" value="" id="eventname"><br>
        <label for="fname">First Name </label><input type="text" name="fname" value="" id="fname"><br>
        <label for="lname">Last Name</label><input type="text" name="lname" value="" id="lname"><br>
        <label for="email">Email*</label><input type="text" name="email" value="" id="email"><br>
        <label for="address">Address</label><input type="text" name="address" value="" id="address"><br>
        <label for="nummer">Phone Number</label><input type="text" name="nummer" value="" id="nummer"><br>
        <label for="end_date">End Date</label><input type="datetime-local" name="end_date" value="" id="end_date"><br>



        <input type="button" name="save" value="Save" id="save" style='width:100px;' onclick="save_form()">
        <input type="button" name="close" value="Close" id="close" style='width:100px;' onclick="close_form()">
        <input type="button" name="delete" value="Delete" id="delete" style='width:100px;' onclick="delete_event()">
    </div>

    <div id="vehicle" class="tabcontent">
        <br>
        <label for="carreg">Car Registration Number</label><input type="text" name="carreg" value="" id="carreg"><br>
        <label for="vin">VIN</label><input type="text" name="vin" value="" id="vin"><br>
        <label for="chassis">Chassis Number</label><input type="text" name="chassis" value="" id="chassis"><br>
        <label for="oil">Oil Inspection</label><input type="text" name="oil" value="" id="oil"><br>
        <label for="modelnum">Model Number</label><input type="text" name="modelnum" value="" id="modelnum"><br>
        <label for="lastserv">Last service</label><input type="text" name="lastserv" value="" id="lastserv"><br>
        <label for="inscom">Insurance Company</label><input type="text" name="inscom" value="" id="inscom"><br>
        <label for="notes">Notes</label><input type="text" name="notes" value="" id="notes"><br><br>

        <input type="button" name="save" value="Save" id="save" style='width:100px;' onclick="save_form()">
        <input type="button" name="close" value="Close" id="close" style='width:100px;' onclick="close_form()">
        <input type="button" name="delete" value="Delete" id="delete" style='width:100px;' onclick="delete_event()">
    </div>
</div>

<div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
    <div class="dhx_cal_navline">
        <div class="dhx_cal_prev_button">&nbsp;</div>
        <div class="dhx_cal_next_button">&nbsp;</div>
        <div class="dhx_cal_today_button"></div>
        <div class="dhx_cal_date"></div>
        <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
        <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
        <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
    </div>
    <div class="dhx_cal_header">
    </div>
    <div class="dhx_cal_data">
    </div>
</div>
</body>

@endsection
<?php

require_once('../js/dhtmlxScheduler/codebase/connector/grid_connector.php');
require_once('../js/dhtmlxScheduler/codebase/connector/db_pdo.php');
require_once('../js/dhtmlxScheduler/codebase/connector/scheduler_connector.php');

$servername = "192.168.10.10";
$username = "homestead";
$password = "secret";

try {
    $dbh = new PDO("mysql:host=$servername;dbname=homestead", $username, $password,array(PDO::ATTR_PERSISTENT => false));
    $conn = new SchedulerConnector($dbh, "PDO");
    $conn->render_table("events","id","start_date,end_date,eventname,priority,fname,lname,email,address,nummer,carreg,vin,chassis,oil,modelnum,lastserv,inscom,notes");
}
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}

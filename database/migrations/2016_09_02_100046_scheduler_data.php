<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchedulerData extends Migration
{

    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('eventname');
            $table->string('priority');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->string('address');
            $table->string('nummer');
            $table->string('carreg');
            $table->string('vin');
            $table->string('chassis');
            $table->string('oil');
            $table->string('modelnum');
            $table->string('lastserv');
            $table->string('inscom');
            $table->string('notes');
        });
    }

    public function down()
    {
        Schema::drop('events');
    }
}